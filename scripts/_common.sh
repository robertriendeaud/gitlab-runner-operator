#!/bin/bash

set -eo pipefail

export OPERATOR_VERSION_CLEAN="${1#v}"
export OPERATOR_VERSION="v$OPERATOR_VERSION_CLEAN"
export RUNNER_REVISION="$2"

# UPSTREAM_UBI_IMAGES_REPOSITORY is the unofficial place Operator UBI images can be pushed/fetched from
export UPSTREAM_UBI_IMAGES_REPOSITORY="registry.gitlab.com/gitlab-org/ci-cd/gitlab-runner-ubi-images"
# UPSTREAM_OPERATOR_IMAGES_REPOSITORY is the place to Operator related images can be pushed/fetched from
export UPSTREAM_OPERATOR_IMAGES_REPOSITORY=${GITLAB_RUNNER_OPERATOR_REGISTRY:-"registry.gitlab.com/gitlab-org/gl-openshift/gitlab-runner-operator"}
# REDHAT_OPERATOR_IMAGES_REPOSITORY is used for public access, so it can be pulled from unauthenticated docker clients
export REDHAT_OPERATOR_IMAGES_REPOSITORY="registry.connect.redhat.com/gitlab"

export GITLAB_RUNNER_CONNECT_NAMESPACE=${GITLAB_RUNNER_CONNECT_NAMESPACE:-scan.connect.redhat.com/ospid-27fdfd59-1d3f-4d28-b8ea-72a20f242c5f}
export GITLAB_RUNNER_CONNECT_REGISTRY_KEY="$GITLAB_RUNNER_CONNECT_REGISTRY_KEY"
export GITLAB_RUNNER_IMAGE="$GITLAB_RUNNER_CONNECT_NAMESPACE/gitlab-runner:$RUNNER_REVISION"

export GITLAB_RUNNER_HELPER_CONNECT_NAMESPACE=${GITLAB_RUNNER_HELPER_CONNECT_NAMESPACE:-scan.connect.redhat.com/ospid-a9ee79b2-9866-4e78-98f4-a80bf701409c}
export GITLAB_RUNNER_HELPER_CONNECT_REGISTRY_KEY="$GITLAB_RUNNER_HELPER_CONNECT_REGISTRY_KEY"
export GITLAB_RUNNER_HELPER_IMAGE="$GITLAB_RUNNER_HELPER_CONNECT_NAMESPACE/gitlab-runner-helper:$RUNNER_REVISION"

export GITLAB_RUNNER_OPERATOR_CONNECT_NAMESPACE=${GITLAB_RUNNER_OPERATOR_CONNECT_NAMESPACE:-scan.connect.redhat.com/ospid-b3e527fb-c0a9-4508-b143-5255abde14aa}
export GITLAB_RUNNER_OPERATOR_UPSTREAM_NAMESPACE=${GITLAB_RUNNER_OPERATOR_UPSTREAM_NAMESPACE:-"$UPSTREAM_OPERATOR_IMAGES_REPOSITORY"}
export GITLAB_RUNNER_OPERATOR_CONNECT_REGISTRY_KEY="$GITLAB_RUNNER_OPERATOR_CONNECT_REGISTRY_KEY"
export GITLAB_RUNNER_OPERATOR_IMAGE="gitlab-runner-operator"

export GITLAB_RUNNER_OPERATOR_BUNDLE_CONNECT_NAMESPACE=${GITLAB_RUNNER_OPERATOR_BUNDLE_CONNECT_NAMESPACE:-scan.connect.redhat.com/ospid-decc0f43-9a9d-4d59-9671-f648e92a99ff}
export GITLAB_RUNNER_OPERATOR_BUNDLE_UPSTREAM_NAMESPACE=${GITLAB_RUNNER_OPERATOR_BUNDLE_UPSTREAM_NAMESPACE:-"$UPSTREAM_OPERATOR_IMAGES_REPOSITORY"}
export GITLAB_RUNNER_OPERATOR_BUNDLE_REGISTRY_KEY="$GITLAB_RUNNER_OPERATOR_BUNDLE_REGISTRY_KEY"
export GITLAB_RUNNER_OPERATOR_BUNDLE_IMAGE="gitlab-runner-operator-bundle"

export GITLAB_RUNNER_OPERATOR_CATALOG_SOURCE_IMAGE_CERTIFIED="gitlab-runner-operator-catalog-source-certified:$OPERATOR_VERSION"
export GITLAB_RUNNER_OPERATOR_CATALOG_SOURCE_IMAGE_UPSTREAM="gitlab-runner-operator-catalog-source:$OPERATOR_VERSION"

export KUBE_RBAC_PROXY_IMAGE=${KUBE_RBAC_PROXY_IMAGE:-registry.redhat.io/openshift4/ose-kube-rbac-proxy@sha256:dc0f91e256c86c3f7cb930d0e4d48eb68576425bc4bd288fb76decb0577c7e9e}

IMAGE=""
KEY=""
IMAGE_VERSION=""

_set_image_and_key() {
    NAME="$1"
    VERSION_OVERRIDE="$2"

    case "$NAME" in
    "runner")
    IMAGE="$GITLAB_RUNNER_IMAGE"
    KEY="$GITLAB_RUNNER_CONNECT_REGISTRY_KEY"
    IMAGE_VERSION="$RUNNER_REVISION"
    ;;
    "helper")
    IMAGE="$GITLAB_RUNNER_HELPER_IMAGE"
    KEY="$GITLAB_RUNNER_HELPER_CONNECT_REGISTRY_KEY"
    IMAGE_VERSION="$RUNNER_REVISION"
    ;;
    "operator")
    KEY="$GITLAB_RUNNER_OPERATOR_CONNECT_REGISTRY_KEY"
    IMAGE_VERSION="${VERSION_OVERRIDE:-$OPERATOR_VERSION}"
    if "$CERTIFIED"; then
      IMAGE="$GITLAB_RUNNER_OPERATOR_CONNECT_NAMESPACE/$GITLAB_RUNNER_OPERATOR_IMAGE:$IMAGE_VERSION"
    else
      IMAGE="$GITLAB_RUNNER_OPERATOR_UPSTREAM_NAMESPACE/$GITLAB_RUNNER_OPERATOR_IMAGE:$IMAGE_VERSION"
    fi
    ;;
    "bundle")
    KEY="$GITLAB_RUNNER_OPERATOR_BUNDLE_REGISTRY_KEY"
    IMAGE_VERSION="${VERSION_OVERRIDE:-$OPERATOR_VERSION}"
    if "$CERTIFIED"; then
      IMAGE="$GITLAB_RUNNER_OPERATOR_BUNDLE_CONNECT_NAMESPACE/$GITLAB_RUNNER_OPERATOR_BUNDLE_IMAGE:$IMAGE_VERSION"
    else
      IMAGE="$GITLAB_RUNNER_OPERATOR_BUNDLE_UPSTREAM_NAMESPACE/$GITLAB_RUNNER_OPERATOR_BUNDLE_IMAGE:$IMAGE_VERSION"
    fi
    ;;
  esac
}

login() {
  KEY="$1"
  echo "$KEY" | >&2 docker login -u unused scan.connect.redhat.com --password-stdin
}

login_and_push() {
  NAME="$1"
  CERTIFIED="$2"
  VERSION_OVERRIDE="$3"
  _set_image_and_key "$NAME" "$VERSION_OVERRIDE"

  if "$CERTIFIED"; then
    if [[ -z "$KEY" ]]; then
    echo "No registry key specified for $NAME, will not push!"
    else
      login "$KEY"
      push_no_fail_if_exists "$IMAGE"
    fi
  else
    push_no_fail_if_exists "$IMAGE"
  fi
}

push_no_fail_if_exists() {
    echo "Pushing Docker image: $1"

    mkdir -p .tmp/
    touch .tmp/docker_stderr

    docker push "$1" 2> .tmp/docker_stderr || echo
    if [[ $(cat .tmp/docker_stderr) == *"The image tag you are pushing already exists"* ]]; then
        echo "Image $1 already exists."
        return 0
    else
        if test "$(wc -l < .tmp/docker_stderr)" -gt 0; then
          cat .tmp/docker_stderr >&2
          return 1
        fi

        return 0
    fi
}

image_digest() {
  _set_image_and_key "$1"

  if [[ -z "$KEY" ]]; then
    >&2 echo "No registry key specified for $IMAGE, will not pull before fetching digest!"
  else
    login "$KEY"
    >&2 docker pull "$IMAGE"
  fi

  DIGEST=$(docker inspect --format='{{ index .RepoDigests 0 }}' "$IMAGE")
  if [[ -z "$DIGEST" ]]; then
    # We have no digest when the previous images are not pushed
    # this is useful for testing the whole script without pushing any images
    echo ":$IMAGE_VERSION"
  else
    DIGEST=$(echo "$DIGEST" | cut -f2 -d"@")
    echo "@$DIGEST"
  fi
}
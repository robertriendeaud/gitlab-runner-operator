## v1.3.0 (2021-08-16)

### Maintenance

- Bump Gitlab Runner version to v14.1.0

## v1.2.0 (2021-07-01)

### New features

- add disconnected environment annotation !80 (Edmund Ochieng @ochienged)

### Maintenance

- Create Community operator resources !74
- Push bundle increment postfix !81

### Other changes

- Update README.md !83

## v1.1.0 (2021-04-22)

### New features

- Support cluster wide operator and cleanup resources !73

### Maintenance

- Don't fail jobs when docker images already exist !77
- Add Mockery and testify packages. Vendor dependencies !76
- Refactor runner controller to allow for easier testing !78

## v1.0.0 (2021-03-22)

### New features

- Allow specifying ImagePullPolicy !55
- set CI_SERVER_TLS_CA_FILE environment variable !52 (Edmund Ochieng @ochienged)

### Maintenance

- Add changelog generation !60
- Update UBI images to latest version to fix helper path !59
- Improve docs and build !57
- refactor registration script !56 (Edmund Ochieng @ochienged)
- refactor registration.sh script !53 (Edmund Ochieng @ochienged)
- Set CSV version and replaced version automatically during the bundle build based on existing tags !51
- Build images not only for tags !50
- Remove and gitignore files touched during bundling and automate related_images creation !49
- Move and improve images building docs. Automate release.yaml creation !48
- use CI_REGISTRY variable !47 (Edmund Ochieng @ochienged)
- Automatic build and deployment of images to RedHat connect !42

### Documentation changes

- Add support and maintenance section !54
- fix docs for release.yaml location !46
